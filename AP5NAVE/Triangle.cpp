#pragma once
#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	this->_points.resize(3, Point(0,0));
	this->_points[0] = a;
	this->_points[1] = b;
	this->_points[2] = c;
}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{
	double area = this->_points[0].getX() * (this->_points[1].getY() - this->_points[2].getY()) +
		this->_points[1].getX() * (this->_points[2].getY() - this->_points[0].getY()) +
		this->_points[2].getX() * (this->_points[0].getY() - this->_points[1].getY());
	return abs(area) / 2;
}

double Triangle::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]) +
		this->_points[1].distance(this->_points[2]) +
		this->_points[0].distance(this->_points[2]);
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
