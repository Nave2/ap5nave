#include "Menu.h"
#include "Menu.h"

Menu::Menu() :
	_shapes(vector<Shape*>())
{
}

Menu::~Menu()
{
}

int Menu::getOption(int min = 0, int max = 3)
{
	int option = -1;
	while (cin >> option, option < min || option > max)
		cout << "Please enter a number between " << min << " to " << max << "." << endl;
	return option;
}

void Menu::drawShapes()
{
	for (int i = 0; i < this->_shapes.size(); i++)
		this->_shapes[i]->draw(this->_canvas);
}

int Menu::print()
{
	cout << "-------------------------------" << endl;
	cout << "Enter 0 to add a new shape." << endl <<
		"Enter 1 to modify or get information from a current shape." << endl <<
		"Enter 2 to delete all of the shapes." << endl <<
		"Enter 3 to exit." << endl;

	int option = getOption();

	switch (option) {
		case 0:
			addShape();
			break;
		case 1:
			modifyShape();
			break;
		case 2:
			deleteShapes();
			break;
		case 3:
			break;
		default:
			cout << "How the hell did you manage to get here ._." << endl;
			break;
	}
	return option;
}

void Menu::addShape()
{
	cout << "-------------------------------" << endl;
	cout << "Enter 0 to add a circle." << endl <<
		"Enter 1 to add an arrow." << endl <<
		"Enter 2 to add a triangle." << endl <<
		"Enter 3 to add a rectangle." << endl;

	int option = getOption();

	Shape* shape = nullptr;
	std::string name, type;

	cout << "name: ";
	cin >> name;

	switch (option) {
	case 0: // circle
	{
		double x, y, radius;
		type = "Circle";
		cout << "x: ";
		cin >> x;
		cout << "y: ";
		cin >> y;
		cout << "radius: ";
		while (cin >> radius, radius < 0)
			cout << "Please enter a non-negative number." << endl;
		shape = new Circle(Point(x, y), radius, type, name);
		break;
	}
	case 1: // arrow
	{
		double x1, y1, x2, y2;
		type = "Arrow";
		cout << "Enter the X of point number: 1" << endl;
		cin >> x1;
		cout << "Enter the Y of point number: 1" << endl;
		cin >> y1;
		cout << "Enter the X of point number: 2" << endl;
		cin >> x2;
		cout << "Enter the Y of point number: 2" << endl;
		cin >> y2;
		shape = new Arrow(Point(x1, y1), Point(x2, y2), type, name);
		break;
	}
	case 2: // triangle
	{
		double x1, y1, x2, y2, x3, y3;
		type = "Triangle";
		cout << "Enter the X of point number: 1" << endl;
		cin >> x1;
		cout << "Enter the Y of point number: 1" << endl;
		cin >> y1;
		cout << "Enter the X of point number: 2" << endl;
		cin >> x2;
		cout << "Enter the Y of point number: 2" << endl;
		cin >> y2;
		cout << "Enter the X of point number: 3" << endl;
		cin >> x3;
		cout << "Enter the Y of point number: 3" << endl;
		cin >> y3;
		shape = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), type, name);
		if (shape->getArea() == 0) { // if all 3 points are on the same line
			cout << "The points entered create a line, no shape created." << endl;
			delete shape;
			return;
		}
		break;
	}
	case 3: // rectangle
	{
		double x, y, length, width;
		type = "Rectangle";
		cout << "Enter the X of the to left corner:" << endl;
		cin >> x;
		cout << "Enter the Y of the to left corner:" << endl;
		cin >> y;
		cout << "Please enter the length of the shape:" << endl;
		cin >> length;
		//abs(length) - that's the way this was handled in the given .exe file
		length = (length > 0) ? length : -length;
		cout << "Please enter the width of the shape:" << endl;
		cin >> width;
		//abs(width) - that's the way this was handled in the given .exe file
		width = (width > 0) ? width : -width;
		shape = new myShapes::Rectangle(Point(x, y), length, width, type, name);
		break;
	}
	default:
		cout << "How the hell did you manage to get here ._." << endl;
		break;
	}

	this->_shapes.push_back(shape);
	shape->draw(this->_canvas);
}

void Menu::modifyShape()
{
	cout << "-------------------------------" << endl;
	int shapesNum = this->_shapes.size();
	if (shapesNum == 0) {
		cout << "No shapes created." << endl;
	}
	else
	{
		cout << "Choose shape:" << endl;
		for (int i = 0; i < shapesNum; i++)
			cout << "Enter " << i << " for " <<
				this->_shapes[i]->getName() << "(" <<
				this->_shapes[i]->getType() << ")" << endl;
		int chosenShape = getOption(0, shapesNum);

		cout << "-------------------------------" << endl;
		cout << "Enter 0 to move the shape." << endl <<
			"Enter 1 to get its details." << endl <<
			"Enter 2 to remove the shape." << endl;
		int option = getOption(0, 2);

		switch (option)
		{
		case 0:
			double x, y;
			cout << "Please enter the X moving scale: ";
			cin >> x;
			cout << "Please enter the Y moving scale: ";
			cin >> y;
			this->_shapes[chosenShape]->clearDraw(this->_canvas);
			this->_shapes[chosenShape]->move(Point(x, y));
			drawShapes();
			break;
		case 1:
			this->_shapes[chosenShape]->printDetails();
			break;
		case 2:
			this->_shapes[chosenShape]->clearDraw(this->_canvas);
			this->_shapes.erase(this->_shapes.begin() + chosenShape);
			drawShapes();
			break;
		default:
			cout << "What?? That's literaly impossible to get here, you bastard!" << endl;
			break;
		}
	}
}

void Menu::deleteShapes()
{
	for (int i = 0; i < this->_shapes.size(); i++)
		this->_shapes[i]->clearDraw(this->_canvas);
	this->_shapes.clear();
}