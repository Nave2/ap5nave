#include "Shape.h"

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
}

void Shape::printDetails() const
{
	cout << "Type: " << this->_type << endl;
	cout << "Name: " << this->_name << endl;
	cout << "Area: " << this->getArea() << endl;
	cout << "Perimeter: " << this->getPerimeter() << endl;

}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}
