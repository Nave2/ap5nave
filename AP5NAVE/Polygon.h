#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	Polygon(const std::string& type, const std::string& name);

	// override functions if need (virtual + pure virtual)
	virtual void move(const Point& other) override;
protected:
	std::vector<Point> _points;
};