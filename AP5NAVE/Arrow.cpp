#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) :
	Shape(name, type), _points(vector<Point>(2, Point(0, 0)))
{
	this->_points[0] = a;
	this->_points[1] = b;
}

Arrow::~Arrow()
{

}

double Arrow::getArea() const
{
	return 0;
}

double Arrow::getPerimeter() const 
{
	return this->_points[0].distance(this->_points[1]);
}

void Arrow::move(const Point& other) 
{
	for (int i = 0; i < this->_points.size(); i++)
		this->_points[i] += other;
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


