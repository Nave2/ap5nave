#include "Circle.h"


Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name):
	Shape(name, type), _center(Point(center.getX(), center.getY()))
{
	this->_radius = radius;
}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return pow(this->_radius, 2) * PI;
}

double Circle::getPerimeter() const
{
	return this->_radius * PI * 2;
}

void Circle::move(const Point& other)
{
	this->_center += other;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


