#include "Menu.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"

void main()
{
	/*
	Circle* c = new Circle(Point(1, 2),100,"Circle", "circle");
	c->printDetails();
	delete c;

	Triangle* t = new Triangle(Point(100, 100), Point(300, 100), Point(150, 0), "Triangle", "tri");
	t->printDetails();
	delete t;

	myShapes::Rectangle* r = new myShapes::Rectangle(Point(100, 100), 100, 200, "Triangle", "tri");
	r->printDetails();
	delete r;

	Arrow* ar = new Arrow(Point(100, 100), Point(200, 350), "Arrow", "arr");
	ar->printDetails();
	delete ar;
	*/
	Menu m;
	int option = 0;
	while (option != 3) // 3 = exit
		option = m.print();
	cout << "Goodbye and thanks for using NE Paint :)" << endl;
	getchar();
}