#pragma once
#include "Shape.h"
#include "Circle.h"
#include "Arrow.h"
#include "Polygon.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Canvas.h"
#include <vector>

class Menu
{
public:

	Menu();
	~Menu();

	// prints opening menu, returns the chosen option
	int print();
	void addShape();
	void modifyShape();
	void deleteShapes();

private: 
	Canvas _canvas;
	vector<Shape*> _shapes;

	void drawShapes();
	int getOption(int min, int max);
};