#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	private:
		// it is possible to manage with only two points but I chose to add this two proporties for the sake of readabillity and efficency...
		double _length;
		double _width;
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();

		// override functions if need (virtual + pure virtual)
		virtual double getArea() const override;
		virtual double getPerimeter() const override;

		virtual void clearDraw(const Canvas& canvas) override;
		virtual void draw(const Canvas& canvas) override;
	};
}