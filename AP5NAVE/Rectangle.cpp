#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	this->_length = length;
	this->_width = width;

	this->_points.resize(2, Point(0, 0));
	this->_points[0] = a;
	this->_points[1] = Point(a.getX() + width, a.getY() + length);
}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * (this->_length + this->_width);
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


