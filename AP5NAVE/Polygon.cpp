#include "Polygon.h"

Polygon::Polygon(const std::string& type, const std::string& name) :
	Shape(name, type), _points(vector<Point>(0, Point(0,0)))
{
}

void Polygon::move(const Point& other)
{
	for (int i = 0; i < this->_points.size(); i++)
		this->_points[i] += other;
}